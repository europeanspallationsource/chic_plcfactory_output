# Assumptions are that ics_plc_factory and chic_plcfactory_output has been cloned to the same parent directory
cd chic_plcfactory_output/ccdb_local

python2.7 generate_ccdb.py

python2.7 ../../ics_plc_factory/plcfactory.py --ccdb=local_file.ccdb.zip --plc-beckhoff --e3 labs-embla_chop-chic-05 -d LabS-Embla:Chop-CHIC-05 --output /home/iocuser/chic_plcfactory_output/+


python2.7 ../../ics_plc_factory/plcfactory.py --ccdb=local_file.ccdb.zip --plc-beckhoff --e3 labs-vip_chop-chic-01 -d LabS-VIP:Chop-CHIC-01 --output /home/iocuser/chic_plcfactory_output/+

LabS-VIP:Chop-CHIC-01
