# some_file.py
import sys

#PLC = "LabS-Embla:Chop-CHIC-05"
#Drive1 = "LabS-Embla:Chop-Drv-0501"
PLC = "LabS-VIP:Chop-CHIC-01"
Drive1 = "LabS-VIP:Chop-Drv-0101"
#Drive2 = "LabS-Embla:Chop-Drv-0502"
#Drive3 = "LabS-Embla:Chop-Drv-0503"
#Drive4 = "LabS-Embla:Chop-Drv-0504"


# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/home/iocuser/ics_plc_factory')

from ccdb_factory import CCDB_Factory
 
factory = CCDB_Factory()
 
# Add the PLC device with name
plc = factory.addBECKHOFF(PLC)
 
# link to definition file, local or in repo, to PLC device
plc.addLink("EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")
plc.setProperty("EPI VERSION", "master")
 
# two devices controlled by the PLC
drive_names = [Drive1]
#drive_names = [Drive1, Drive2, Drive3, Drive4]
 
# add sub devices to PLC if needed
plc.setControls(drive_names)
 
 
#Go through all the devices in cmses
for d in drive_names:
    drive = factory.addDevice("DRV_TYPE", d)
    drive.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")
    drive.setProperty("EPI VERSION", "master")
 
# Dump our CCDB
factory.dump("local_file")






#from ccdb_factory import CCDB_Factory
 
#factory = CCDB_Factory()
 
# Add the PLC device with name
#plc = factory.addBECKHOFF("LabS-Embla:Chop-CHIC-04")
 
# link to definition file, local or in repo, to PLC device
#factory.addLink("chic_type", "EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")
 
# two devices controlled by the PLC
#drives = ["LabS-Embla:Chop-Drv-0401"]
 
# add sub devices to PLC if needed
#plc.setControls(drives)
 
#Go through all the devices in cmses
#for drv in drives:
#    factory.addDevice("chic_type", drv)
      
 
#factory.device("LabS-Embla:Chop-Drv-0401").addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")
 
# Dump our CCDB
