#!/usr/bin/env python2

import os
import sys

sys.path.append(os.path.curdir)
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()

# External links for CHOP_DRV
factory.addLink("CHOP_DRV", "EPI", "https://bitbucket.org/europeanspallationsource/chop_drv")

#
# Adding device LabS-VIP:Chop-CHIC-03 of type CHOP_CHIC
#
root = factory.addDevice("CHOP_CHIC", "LabS-VIP:Chop-CHIC-03")
# Properties
root.setProperty("EPICSModule", "['labs-vip_chop-chic-03']")
root.setProperty("EPICSSnippet", "['labs-vip_chop-chic-03']")
root.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "12288")
root.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
root.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
root.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
root.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
root.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "2000")
root.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "LittleEndian")
root.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "16#40")
root.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "1")
root.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "10")
root.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "10")
root.setProperty("EPI VERSION", "master")
# External links
root.addLink("EPI", "https://bitbucket.org/europeanspallationsource/chop_chic")

#
# Adding device LabS-VIP:Chop-Drv-0301 of type CHOP_DRV
#
dev = root.addDevice("CHOP_DRV", "LabS-VIP:Chop-Drv-0301")
# Properties
dev.setProperty("EPI VERSION", "master")


#
# Saving the created CCDB
#
factory.save("LabS-VIP:Chop-CHIC-03")
