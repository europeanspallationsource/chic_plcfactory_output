#!/usr/bin/env python2

import os
import sys

sys.path.append(os.path.curdir)
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()


#
# Adding Beckhoff PLC: LOKI-ChpSy1:Chop-CHIC-001
#
plc = factory.addBECKHOFF("LOKI-ChpSy1:Chop-CHIC-001")
# Properties
plc.setProperty("EPICSModule", "[]")
plc.setProperty("EPICSSnippet", "[]")
plc.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "12288")
plc.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "2000")
plc.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "LittleEndian")
plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "16#40")
plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "1")
plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "10")
plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "10")
plc.setProperty("EPI VERSION", "master")
# External links
plc.addLink("EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")

#
# Adding device LOKI-ChpSy1:Chop-BWC-101 of type DRV_TYPE
#
dev = plc.addDevice("DRV_TYPE", "LOKI-ChpSy1:Chop-BWC-101")
# Properties
dev.setProperty("EPI VERSION", "master")
# External links
dev.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")

#
# Adding device LOKI-ChpSy1:Chop-BWC-102 of type DRV_TYPE
#
dev = plc.addDevice("DRV_TYPE", "LOKI-ChpSy1:Chop-BWC-102")
# Properties
dev.setProperty("EPI VERSION", "master")
# External links
dev.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")

#
# Adding device LOKI-ChpSy1:Chop-RIC-101 of type DRV_TYPE
#
dev = plc.addDevice("DRV_TYPE", "LOKI-ChpSy1:Chop-RIC-101")
# Properties
dev.setProperty("EPI VERSION", "master")
# External links
dev.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")

#
# Adding device LOKI-ChpSy1:Chop-RIC-102 of type DRV_TYPE
#
dev = plc.addDevice("DRV_TYPE", "LOKI-ChpSy1:Chop-RIC-102")
# Properties
dev.setProperty("EPI VERSION", "master")
# External links
dev.addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")


#
# Saving the created CCDB
#
factory.save("LOKI-ChpSy1:Chop-CHIC-001")
